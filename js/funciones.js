
// Evento que se ejecuta al pulsar el botón agregar
document.querySelector("input[type=submit]").addEventListener("click",function(e){
    // cancelamos el evento
    e.preventDefault();
 
    const nombre=document.querySelector("input[name=nombre]");
    const apellidos=document.querySelector("input[name=apellidos]");
    const correoElectronico=document.querySelector("input[name=correoElectronico]");
    const telefono=document.querySelector("input[name=telefono]");
    const message=document.querySelector("textarea[name=message]");
 
     
    agregarFila(nombre.value, apellidos.value, correoElectronico.value, telefono.value, message.value);
 
    nombre.value="";
    apellidos.value="";
    correoElectronico.value="";
    telefono.value="";
    message.value="";
    nombre.focus();

});

function agregarFila(nombre, apellidos, correoElectronico, telefono, message) {
    // añadimos el contacto a la tabla crando el tr, td's
    const tr=document.createElement("tr");
 
    const tdNombre=document.createElement("td");
    let txt=document.createTextNode(nombre);
    tdNombre.appendChild(txt);
 
    const tdApellidos=document.createElement("td");
    txt=document.createTextNode(apellidos);
    tdApellidos.appendChild(txt);

    const tdcorreoElectronico=document.createElement("td");
    txt=document.createTextNode(correoElectronico);
    tdcorreoElectronico.appendChild(txt);

    const tdtelefono=document.createElement("td");
    txt=document.createTextNode(telefono);
    tdtelefono.appendChild(txt);

    const tdmessage=document.createElement("td");
    txt=document.createTextNode(message);
    tdmessage.appendChild(txt);
 
 
    tr.appendChild(tdNombre);
    tr.appendChild(tdApellidos);
    tr.appendChild(tdcorreoElectronico);
    tr.appendChild(tdtelefono);
    tr.appendChild(tdmessage);

 
    const tbody=document.getElementById("listado").querySelector("tbody").appendChild(tr);
 
}